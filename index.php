<?php require_once "./code.php"

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    .fullAddress {
      border: 2px solid #000;
      width: 70%;
      height: 40vh;
      margin: auto;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    .getGrades {
      border: 2px solid #000;
      width: 70%;
      height: 50vh;
      margin: 5rem auto;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
  </style>
</head>

<body>

  <div class="fullAddress">
    <h1>Full Address </h1>
    <p> <?= getFullAddress('3F Caswynn Bldg., Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines') ?></p>
    <p> <?= getFullAddress('3F Enzo Bldg., Buendia Avenue', 'MakatiCity', 'Metro Manila', 'Philippines') ?></p>
  </div>

  <div class="getGrades">
    <h1>Letter-Based Grading </h1>
    <p> <?= getLetterGrade(100) ?></p>
    <p> <?= getLetterGrade(87) ?></p>
    <p> <?= getLetterGrade(94) ?></p>
    <p> <?= getLetterGrade(74) ?></p>

  </div>

</body>

</html>